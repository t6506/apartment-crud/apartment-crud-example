package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.data.Apartment;
import org.example.manager.ApartmentManager;
import org.example.search.SearchRequest;

import java.util.List;

@Slf4j
public class Main {
    public static void main(String[] args) {
        ApartmentManager manager = new ApartmentManager();

        Apartment threeRoomsApartment1 = new Apartment(0, "three rooms", 6_000_000, 85_0, true, false, 10, 10);
        Apartment oneRoomsApartment1 = new Apartment(0, "one room", 2_600_000, 38_0, false, false, 3, 9);
        Apartment twoRoomsApartment1 = new Apartment(0, "two rooms", 4_700_000, 65_0, true, false, 5, 9);
        Apartment twoRoomsApartment2 = new Apartment(0, "two rooms", 4_600_000, 63_0, true, false, 4, 9);
        Apartment threeRoomsApartment2 = new Apartment(0, "three rooms", 5_900_000, 83_5, true, true, 1, 10);
        Apartment oneRoomsApartment2 = new Apartment(0, "one room", 2_500_000, 36_5, true, false, 2, 5);
        Apartment twoRoomsApartment3 = new Apartment(0, "two rooms", 4_500_000, 60_0, true, false, 5, 5);
        manager.create(oneRoomsApartment1);
        manager.create(oneRoomsApartment2);
        manager.create(twoRoomsApartment1);
        manager.create(twoRoomsApartment2);
        manager.create(twoRoomsApartment3);
        manager.create(threeRoomsApartment1);
        manager.create(threeRoomsApartment2);


        SearchRequest request1 = new SearchRequest();
        request1.setLoggia(true);
        SearchRequest request2 = new SearchRequest();
        int areaMin = 60_0;
        int areaMax = 70_0;
        request2.setAreaMin(areaMin);
        request2.setAreaMax(areaMax);
        SearchRequest emptyDTO = new SearchRequest();

        List<Apartment> search1 = manager.searchBy(request1);
        StringBuilder example1 = new StringBuilder("\n\nРезультаты поиска квартир с лоджией:");
        for (Apartment apartment: search1) {
            example1.append("\n").append(apartment.toString());
        }
        example1.append("\n\n");
        log.info(example1.toString());


        List<Apartment> search2 = manager.searchBy(request2);
        StringBuilder example2 = new StringBuilder("\n\nРезультаты поиска квартир с площадью от " + areaMin + " до "+ areaMax + " кв.м/10:");
        for (Apartment apartment: search2) {
             example2.append("\n").append(apartment.toString());
        }
        example2.append("\n");
        log.info(example2.toString());

        List<Apartment> search3 = manager.searchBy(emptyDTO);
        StringBuilder example3 = new StringBuilder("\n\nРезультаты поиска квартир по пустому запросу - полный список квартир:");
        for (Apartment apartment: search3) {
            example3.append("\n").append(apartment.toString());
        }
        example3.append("\n");
        log.info(example3.toString());
    }
}
